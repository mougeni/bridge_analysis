---
title: 'R package for equivalence of medical devices: EquiDevice'
author: "Fabrice"
date: '2023-04-04'
output:
  pdf_document: default
  github_document: default
---




# Description

This is an R package to compare an established, quality-controlled biomedical analyzer in use with a new machine, in order to assess equivalence. Equivalency is assumed when the tolerance limits from the bivariate least square regression analysis are within pre-defined acceptance limits and the bias is less than the predefined acceptable bias limit.


# Installation

Since this a development version, it is only available in remote version of Gitlab. To download, please enter the following code to install from the repository in Gitlab. This is a temporary link, but will be changed in the next revision.


```r
devtools::install_gitlab("mougeni/EquiDevice")
```


# Usage

After installing the package, two functions are needed: clean_data() and render_report. The former will help you in importing the data and transforming that to the right format to be used in the internal functions. The latter will perform the analysis and render the report.

This can be done as follows:

* **Step 1**: Call the function clean_data()

The argument in the function should be the path of the file where data are stored and the name of the file. For now this should be xlsx, but will change later, and make sure that the data should be the wide format.


<!-- * Data columns -->

<!-- \includegraphics[height= 1.5 cm]{"formdata.png"} -->



The function can be called as follows:

```r
clean_data("data/analyse des automates.xlsx")
```


Then make sure that you have a new csv file created called "analyse_automates". This will ensure that the render function will work as you have the right name to be used in the internal functions.


* **Step 2**: Call the function render_report()

Launch the following code to obtain the report with the analysis performed. Here we aimed to compare a new machine Yumizen to replace the old one called Pentra. You need to specify the path where the raw data are stored. This is done as follows:


```r
render_report(path_data = "data/analyse des automates.xlsx")
```



\includegraphics{"repcap1.png"}


